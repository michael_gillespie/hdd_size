Set-Location "C:\Users\$env:USERNAME\Google Drive\programs\hdd_size"

# https://www.howtogeek.com/324956/how-much-free-space-should-you-leave-on-your-windows-pc/
$freeSpace = 32008305664 # 25%

# get computer names
$computer_names = Get-Content .\machines_to_check.txt

[System.Collections.ArrayList]$usersToReDo = @()

foreach($computer in $computer_names) {
  $usercount = 0
  Try {
    # get user disk size and free space
    $info = .\PSTools\PsExec.exe -nobanner \\$computer wmic logicaldisk get size,freespace,caption

    if ($Error[$usercount] -match "error code 0") {
      $count = 0
      while ($count -lt 12) {
        $info[2] -match "\d{$count}"
        $count++
      }

      if ($Matches[0] -lt $freeSpace) {
        echo $computer >> .\enough_space_no.txt
        $info[2] >> .\enough_space_no.txt
        echo "" >> .\enough_space_no.txt
        Write-Host "$computer : not enough space"
      }
      else {
        echo $computer >> .\enough_space_yes.txt
        $info[2] >> .\enough_space_yes.txt
        echo "" >> .\enough_space_yes.txt
        Write-Host "$computer : enough space"
      }
    }
    else {
      $usersToRedo += $computer
      Write-Host "$computer : recheck"
    }
    
    $usercount++
  }
  Catch {
    echo "$computer error"
  }
}

# create new machines_to_check.txt
Remove-Item .\machines_to_check.txt
echo $usersToReDo >> .\machines_to_check.txt
