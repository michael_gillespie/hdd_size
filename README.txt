what does it do?
hdd_size-v*.ps1 remotely checks the size and free space of a hdd from a list of names.

skills used:
> powershell
> PsTools

requirements:
> powershell
> PsTools

how to run:
1. put list of machine names into machines_to_check.txt
2. run hdd_size-v*.ps1
3. enough_space_yes/no are created with results
